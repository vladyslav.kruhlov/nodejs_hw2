const bcrypt = require('bcryptjs');
const { User } = require('./models/Users');
const { Note } = require('./models/Notes');

const getMyProfileInfo = (req, res) => {
  return res.status(200).json({
    user: {
      _id: req.user.userId,
      username: req.user.username,
      createdDate: new Date(),
      name: req.user.name,
    },
  });
};

const deleteMyProfile = async (req, res) => {
  const deletedNote = await Note.deleteMany({
    userId: req.user.userId,
  })

  if (deletedNote) {
    return  User.findByIdAndDelete(req.user.userId)
      .then((user) => {
        if (user) {
          return res.status(200).send({
            message: 'Success',
            status: 200,
          });
        }

        return res.status(400).send({
          message: 'Something wrong',
        });
      });
  }

  return res.status(400).send({
    message: 'Something wrong'
  })
};

const changeMyPassword = async (req, res) => {
  const user = await User.findById(req.user.userId);
  const { oldPassword, newPassword } = req.body;

  if (!oldPassword || !newPassword) {
    return res.status(400).send({
      message: 'string',
    });
  }

  if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
    user.password = await bcrypt.hash(newPassword, 10);

    return user.save().then(() => res.status(200).send({
      message: 'Success',
      status: 200
    }));
  }

  return res.status(400).send({
    message: 'wrong old password',
  });
};

module.exports = {
  getMyProfileInfo,
  deleteMyProfile,
  changeMyPassword,
};
