const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://weeny:test@sandbox.xq1id.mongodb.net/?retryWrites=true&w=majority');

const { filesRouter } = require('./filesRouter');
const { booksRouter } = require('./booksRouter');
const { usersRouter } = require('./usersRouter');
const { notesRouter } = require('./notesRouter');
const { authRouter } = require('./authRouter');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', filesRouter);
app.use('/api/books', booksRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
// eslint-disable-next-line no-use-before-define
app.use(errorHandler);

function errorHandler(err, req, res) {
  res.status(500).send({ message: 'Server error', info: err });
}
