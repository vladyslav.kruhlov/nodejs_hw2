const { Book } = require('./models/Books');

function createBook(req, res) {
  const { title, author, userId } = req.body;
  const book = new Book({
    title,
    author,
    userId,
  });
  book.save().then((saved) => {
    res.json(saved);
  });
}

function getBooks(req, res) {
  return Book.find().then((result) => {
    res.json(result);
  });
}

const getBook = (req, res) => Book.findById(req.params.id)
  .then((book) => {
    res.json(book);
  });

const updateBook = async (req, res) => {
  const book = await Book.findById(req.params.id);
  const { title, author } = req.body;

  if (title) book.title = title;
  if (author) book.author = author;

  return book.save().then((saved) => res.json(saved));
};

const deleteBook = (req, res) => Book.findByIdAndDelete(req.params.id)
  .then((book) => {
    res.json(book);
  });

const getMyBooks = (req, res) => {
  return Book.find({ userId: req.user.userId }, '-__v').then((result) => {
    res.json(result);
  });
};

const updateMyBookById = (req, res) => {
  const { title, author } = req.body;
  return Book.findByIdAndUpdate({
    _id: req.params.id,
    userId: req.user.userId,
  }, { $set: { title, author } })
    .then(() => {
      res.json({ message: 'Book was updated' });
    });
};

const markMyBookCompletedById = (req, res) => {
  return Book.findByIdAndUpdate({
    _id: req.params.id,
    userId: req.user.userId,
  }, { $set: { completed: true } })
    .then(() => {
      res.json({ message: 'Book was marked completed' });
    });
};

module.exports = {
  createBook,
  getBooks,
  getBook,
  updateBook,
  deleteBook,
  getMyBooks,
  updateMyBookById,
  markMyBookCompletedById,
};
