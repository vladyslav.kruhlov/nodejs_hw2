const express = require('express');

const router = express.Router();
const { getMyProfileInfo, deleteMyProfile, changeMyPassword } = require('./usersService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/me', authMiddleware, getMyProfileInfo);

router.delete('/me', authMiddleware, deleteMyProfile);

router.patch('/me', authMiddleware, changeMyPassword);

module.exports = {
  usersRouter: router,
};
