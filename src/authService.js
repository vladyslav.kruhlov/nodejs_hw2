const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

const registerUser = async (req, res, next) => {
    const { name, username, password } = req.body;

    const user = new User({
      name: name || '',
      username,
      password: await bcrypt.hash(password, 10),
    });

    user.save()
      .then(() => {
        return res.json({
          message: 'Success',
          status: 200,
        })
      })
      .catch((err) => {
        if (err.name === 'MongoServerError' && err.code === 11000) {
          return res.status(400).json({
            message: 'Username already in use.',
            data: { err },
          });
        }

        next(err);
      });
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    // eslint-disable-next-line no-underscore-dangle
    const payload = { username: user.username, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.json({
      message: 'Success',
      jwt_token: jwtToken,
      status: 200,
    });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  loginUser,
  registerUser,
};
