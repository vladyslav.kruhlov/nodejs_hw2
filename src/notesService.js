const { Note } = require('./models/Notes');

const addNote = (req, res) => {
  const { text } = req.body;

  if (!text) {
    return res.status(400).send({
      message: 'wrong parameters',
    });
  }

  const note = new Note({
    text,
    userId: req.user.userId,
    completed: false,
    createdDate: new Date(),
  });

  return note.save().then((note) => res.json({
    message: 'Success',
    status: 200,
    note,
  }));
};

const getMyNotes = (req, res) => {
  const { offset, limit } = req.query;

  return Note.find({ userId: req.user.userId }, null, { skip: offset || 0, limit: limit || 0 })
    .then((notes) => res.json({
      offset: +offset || 0,
      limit: +limit || 0,
      count: notes.length,
      notes,
    }));
};

const getNoteById = (req, res, next) => Note.findOne({
  _id: req.params.id,
  userId: req.user.userId,
}).then((result) => {
  if (!result) {
    return res.status(400).send({
      message: 'We cannot find such a note',
    });
  }

  return res.json({
    note: result,
  });
})
  .catch((err) => next(err));

const editNote = (req, res, next) => {
  const { text } = req.body;

  if (!text && typeof text !== 'string') {
    return res.status(400).send({
      message: 'please provide text parameter',
    });
  }

  return Note.findOneAndUpdate({
    _id: req.params.id,
    userId: req.user.userId,
  }, { $set: { text } })
    .then((note) => {
      if (!note) {
        return res.status(400).send({
          message: 'cannot find such a file to update',
        });
      }

      return res.json({
        message: 'Success',
        status: 200,
      });
    })
    .catch((err) => next(err));
};

const checkNote = async (req, res, next) => {
  const note = await Note.findOne({
    _id: req.params.id,
    userId: req.user.userId,
  });

  if (note === null) {
    return res.status(400).send({
      message: 'cannot find such a file to (un)check',
    });
  }

  const completed = !note.completed;

  return Note.findOneAndUpdate({
    _id: req.params.id,
    userId: req.user.userId,
  }, { $set: { completed } }).then((result) => {
    if (!result) {
      return res.status(400).send({
        message: 'cannot find such a file to (un)check',
      });
    }

    return res.json({
      message: 'Success',
      status: 200,
    });
  }).catch((err) => next(err));
};

const deleteNote = (req, res, next) => Note.findOneAndDelete({
  _id: req.params.id,
  userId: req.user.userId,
}).then((note) => {
  if (!note) {
    return res.status(400).send({
      message: 'cannot find such a file to (un)check',
    });
  }

  return res.json({
    message: 'Success',
    status: 200,
  });
}).catch((err) => next(err));

module.exports = {
  addNote,
  getMyNotes,
  getNoteById,
  editNote,
  checkNote,
  deleteNote,
};
