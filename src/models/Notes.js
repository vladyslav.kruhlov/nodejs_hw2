const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: false,
  },
  createdDate: {
    type: Date,
    required: true,
  },
});

const Note = mongoose.model('Note', noteSchema);

module.exports = {
  Note,
};
