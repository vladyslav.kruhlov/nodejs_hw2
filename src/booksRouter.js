const express = require('express');

const router = express.Router();
const {
  createBook, getBooks, getBook, deleteBook, updateBook, getMyBooks, updateMyBookById,
  markMyBookCompletedById
} = require('./booksService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createBook);

router.get('/', authMiddleware, getBooks);

router.get('/get-my-books', authMiddleware, getMyBooks);

router.get('/:id', getBook);

router.patch('/update-my-book/:id', authMiddleware, updateMyBookById);

router.patch('/mark-completed/:id', authMiddleware, markMyBookCompletedById);

router.patch('/:id', updateBook);

router.delete('/:id', deleteBook);

module.exports = {
  booksRouter: router,
};
