const express = require('express');
const {
  addNote,
  getMyNotes,
  getNoteById,
  editNote,
  checkNote,
  deleteNote,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

const router = express.Router();

router.get('/', authMiddleware, getMyNotes);

router.get('/:id', authMiddleware, getNoteById);

router.post('/', authMiddleware, addNote);

router.put('/:id', authMiddleware, editNote);

router.patch('/:id', authMiddleware, checkNote);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
