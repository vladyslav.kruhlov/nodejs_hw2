export interface User {
  name: string;
  username: string;
  password: string;
}

export interface getUser {
  user: {
    username: string;
    _id: string;
    createdDate: string;
    name: string;
  }
}

export interface Response {
  message: string;
  status: number;
  note?: Note;
}

export interface Note {
  _id: string;
  text: string;
  userId: string;
  completed: boolean;
  createdDate: string;
}

export interface AllNotes {
  offset: number;
  limit: number;
  count: number;
  notes: Note[];
}

export interface UpdateNoteEvent {
  old: Note;
  new: {
    text: string;
    create: boolean;
  };
}

export interface CreateNewItem extends Note {
  create: boolean;
}
