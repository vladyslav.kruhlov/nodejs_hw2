import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup( {
    name: new FormControl<string>( '' ),
    username: new FormControl<string>( '' ),
    password: new FormControl<string>( '' ),
  } )

  constructor(
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    private router: Router) {
  }

  ngOnInit(): void {
    if(localStorage.getItem('jwt-token')) {
      this.router.navigate(['']);
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  closeSnackBar() {
    this._snackBar.dismiss()
  }

  onSubmit(): Subscription {
    const body: User = {
      name: this.registerForm.value.name || '',
      username: this.registerForm.value.username || '',
      password: this.registerForm.value.password || '',
    }

    return this.http.post<any>( 'http://localhost:8080/api/auth/register', body ).subscribe({
      next: response => {
        console.log(response)
        if(response.status === 200) {
          this.openSnackBar('User successfully registered. You can login', 'Hide')
          setTimeout(() => this.closeSnackBar(), 3000)
          this.router.navigate(['/login'])
        }
      },
      error: error => {
        this.openSnackBar(error.error.message || 'Something went wrong', 'Hide')
        console.log(error)
      }
    })
  }
}
