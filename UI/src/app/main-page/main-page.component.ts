import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AllNotes, CreateNewItem, getUser, Note, UpdateNoteEvent, Response } from '../../models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ModelPasswordComponent } from '../model-password/model-password.component';
import { ModalDeleteComponent } from '../modal-delete/modal-delete.component';
import { ModalDeleteNoteComponent } from '../modal-delete-note/modal-delete-note.component';
import { FormControl } from '@angular/forms';

@Component( {
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
} )
export class MainPageComponent implements OnInit {

  name: string = '';
  username: string = '';
  search = new FormControl('');
  notes: Note[] = [];
  searchArray: Note[] = [];

  constructor(
    private router: Router,
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    if (!localStorage.getItem( 'jwt-token' )) {
      this.router.navigate( ['/login'] )
    }

    this.http.get<getUser>( 'http://localhost:8080/api/users/me', {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      } )
    } ).subscribe( {
      next: ( res ) => {
        this.username = res.user.username;
        this.name = res.user.name;
      },
      error: () => {
        this.openSnackBar( 'Something went wrong. Try again later!', 'Hide' )
        this.router.navigate( ['/login'] );
      }
    } )

    this.http.get<AllNotes>( 'http://localhost:8080/api/notes', {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      } )
    } ).subscribe( {
      next: ( res ) => {
        this.notes = res.notes
        this.searchArray = [...this.notes]
      },
      error: ( err ) => {
        console.log( err )
        this.openSnackBar( 'We cannot get your notes. Try again later!', 'Hide' )
      }
    } )
  }

  openSnackBar( message: string, action: string ) {
    this._snackBar.open( message, action );
  }

  closeSnackBar() {
    this._snackBar.dismiss()
  }

  openDialog( enterAnimationDuration: string, exitAnimationDuration: string ): void {
    this.dialog.open( ModelPasswordComponent, {
      width: '600px',
      height: '260px',
      enterAnimationDuration,
      exitAnimationDuration,
    } );
  }

  openDelete( enterAnimationDuration: string, exitAnimationDuration: string ): void {
    this.dialog.open( ModalDeleteComponent, {
      width: '250px',
      enterAnimationDuration,
      exitAnimationDuration,
    } );
  }

  logout() {
    localStorage.removeItem( 'jwt-token' )
    this.router.navigate( ['/login'] );
  }

  deleteItem( item: Note ) {
    const index = this.searchArray.indexOf( item )
    const dialogRef = this.dialog.open( ModalDeleteNoteComponent, {
      width: '250px',
      enterAnimationDuration: '0ms',
      exitAnimationDuration: '0ms',
      data: item,
    } );

    dialogRef.afterClosed().subscribe( res => {
      if (res) {
        this.http.delete<Response>( `http://localhost:8080/api/notes/${item._id}`, {
          headers: new HttpHeaders( {
            'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
          } )
        } ).subscribe( {
          next: ( res ) => {
            if (res.status === 200) {
              this.searchArray.splice( index, 1 )
              this.openSnackBar( 'Note was successfully deleted', 'Hide' )
              setTimeout( () => this.closeSnackBar(), 4000 )
            }
          },
          error: () => {
            this.openSnackBar( 'Something went wrong', 'Hide' )
            setTimeout( () => this.closeSnackBar(), 4000 )
          }
        } )
      }
    } )
  }

  updateItem( updateItems: UpdateNoteEvent ) {
    this.http.put<Response>( `http://localhost:8080/api/notes/${updateItems.old._id}`, {
      text: updateItems.new.text,
    }, {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      })
    } ).subscribe( {
      next: ( res ) => {
        if (res.status === 200) {
          const index = this.searchArray.indexOf( updateItems.old );
          this.searchArray[index].text = updateItems.new.text;
        }
      },
      error: (err) => {
        this.openSnackBar( 'Something went wrong', 'Hide' )
        setTimeout( () => this.closeSnackBar(), 4000 )
        console.log(err)
      }
    } )
  }

  createNote( newItem: CreateNewItem|Note ) {
    const token = localStorage.getItem( 'jwt-token' )

    return this.http.post<Response>( 'http://localhost:8080/api/notes', {
        text: newItem.text
      },
      {
        headers: new HttpHeaders( {
          'Authorization': `Bearer ${token}`
        } )
      } ).subscribe( {
      next: ( res ) => {
        if (res.status === 200 && res.note) {
          this.searchArray.push( res.note )
        }
      },
      error: () => {
        this.openSnackBar( 'Something went wrong', 'Hide' )
        setTimeout( () => this.closeSnackBar(), 4000 )
      }
    } )
  }

  searchNote() {
    let findMatches = false;
    console.log(this.search.value)

    for (let i = 0; i < this.notes.length; i++) {
      if (this.search.value === this.notes[i]._id) {
        console.log('find')
        findMatches = true
        this.searchArray =  [this.notes[i]]
      }
    }

    if(!findMatches) {
      this.searchArray = []
    }

    if(this.search.value === '') {
      this.searchArray = [...this.notes]
    }
  }

}
