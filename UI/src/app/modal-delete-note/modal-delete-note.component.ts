import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-delete-note',
  templateUrl: './modal-delete-note.component.html',
  styleUrls: ['./modal-delete-note.component.scss']
})
export class ModalDeleteNoteComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ModalDeleteNoteComponent>) { }

  ngOnInit(): void {
  }

  ok() {
    this.dialogRef.close(true)
  }

  reject() {
    this.dialogRef.close(false)
  }

}
