import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Response } from '../../models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss']
})
export class ModalDeleteComponent implements OnInit {

  constructor(
    public dialog: MatDialogRef<ModalDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  closeSnackBar() {
    this._snackBar.dismiss()
  }

  ok() {
    return this.http.delete<Response>('http://localhost:8080/api/users/me', {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem('jwt-token')
      })
    }).subscribe({
      next: (res) => {
        if (res.status === 200) {
          this.openSnackBar(res.message, 'Hide')
          localStorage.removeItem('jwt-token')
          this.router.navigate(['/login'])
          setTimeout(() => this.closeSnackBar(), 3000);
          this.dialog.close()
        }
      },
      error: () => {
        localStorage.removeItem('jwt-token')
        this.openSnackBar('something went wrong', 'Hide');
        setTimeout(() => this.closeSnackBar(), 3000);
      }
    })
  }

  exit() {
    this.dialog.close();
  }

}
