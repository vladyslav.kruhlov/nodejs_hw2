import { Component, Inject, OnInit } from '@angular/core';
import { Note } from '../../models';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-modal-note',
  templateUrl: './modal-note.component.html',
  styleUrls: ['./modal-note.component.scss']
})
export class ModalNoteComponent implements OnInit {

  isNewItem!: boolean
  text = new FormControl(this.data?.text);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Note|undefined,
    private dialogRef: MatDialogRef<ModalNoteComponent>
  ) { }

  ngOnInit(): void {
    this.isNewItem = !this.data;
  }

  editNote() {
    this.dialogRef.close({
      text: this.text.value,
      create: false,
    })
  }

  createNote() {
    this.dialogRef.close({
      text: this.text.value,
      _id: '',
      userId: '',
      createdDate: '',
      completed: false,
      create: true,
    })
  }

}
