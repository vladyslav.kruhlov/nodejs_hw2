import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalNoteComponent } from '../modal-note/modal-note.component';
import { CreateNewItem, Note, UpdateNoteEvent } from '../../models';

@Component( {
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss']
} )
export class NotesListComponent implements OnInit {

  @Input() notes!: Note[];
  @Output() delete: EventEmitter<Note> = new EventEmitter<Note>();
  @Output() update: EventEmitter<UpdateNoteEvent> = new EventEmitter<UpdateNoteEvent>();
  @Output() create: EventEmitter<CreateNewItem> = new EventEmitter<CreateNewItem>();

  constructor( public dialog: MatDialog ) {
  }

  ngOnInit(): void {
  }

  onDelete( item: Note ) {
    this.delete.emit( item )
  }

  openDialog( enterAnimationDuration: string, exitAnimationDuration: string, item?: Note ): void {
    const dialogRef = this.dialog.open( ModalNoteComponent, {
      width: '900px',
      height: '600px',
      enterAnimationDuration,
      exitAnimationDuration,
      data: item,
    } );

    dialogRef.afterClosed().subscribe( res => {
      if (res && !res.create && item) {
        this.update.emit( {
          old: item,
          new: res,
        } )
      } else if(res) {
        this.create.emit( { ...res } )
      }
    } )
  }

}
