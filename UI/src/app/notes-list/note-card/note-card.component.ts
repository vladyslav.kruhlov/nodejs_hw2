import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Note } from '../../../models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.scss']
})
export class NoteCardComponent implements OnInit {

  @Input() item!: Note
  @Output() clickedDeleteButton: EventEmitter<any> = new EventEmitter<any>();

  complete = false;

  constructor(private http: HttpClient, private _snackBar: MatSnackBar,) { }

  ngOnInit(): void {
    this.complete = this.item.completed
  }

  onDeleteClicked(event: MouseEvent) {
    event.stopPropagation()
    this.clickedDeleteButton.emit()
  }

  onComplete( event: MouseEvent ) {
    event.stopPropagation()
  }

  openSnackBar( message: string, action: string ) {
    this._snackBar.open( message, action );
  }

  closeSnackBar() {
    this._snackBar.dismiss()
  }

  onChange() {
    this.complete = !this.complete;
    return this.http.patch<Response>(`http://localhost:8080/api/notes/${this.item._id}`, {} , {
      headers: new HttpHeaders( {
        'Authorization': 'Bearer ' + localStorage.getItem( 'jwt-token' )
      })
    }).subscribe({
      next: (res) => {
        if(res.status === 200) {
        }
      },
      error: () => {
        this.complete = !this.complete
        this.openSnackBar( 'Something went wrong', 'Hide' )
        setTimeout( () => this.closeSnackBar(), 4000 )
      }
    })
  }

}
