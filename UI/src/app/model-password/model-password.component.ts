import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '../../models';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-model-password',
  templateUrl: './model-password.component.html',
  styleUrls: ['./model-password.component.scss']
})
export class ModelPasswordComponent implements OnInit {

  changePasswordForm = new FormGroup({
    oldPassword: new FormControl(''),
    newPassword: new FormControl(''),
    confirmPassword: new FormControl(''),
  });

  constructor(
    private _snackBar: MatSnackBar,
    private http: HttpClient,
    private dialogRef: MatDialogRef<ModelPasswordComponent>
    ) { }

  ngOnInit(): void {
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  closeSnackBar() {
    this._snackBar.dismiss()
  }

  onSubmit() {
    const oldPassword: string = this.changePasswordForm.value.oldPassword!;
    const newPassword: string = this.changePasswordForm.value.newPassword!;
    const confirmPassword = this.changePasswordForm.value.confirmPassword;

    if (newPassword !== confirmPassword) {
      this.openSnackBar('Your passwords don\'t match', 'Hide');
      setTimeout(() => this.closeSnackBar(), 3000)
      this.changePasswordForm.setValue({
        oldPassword,
        newPassword,
        confirmPassword: '',
      })
      return null;
    }

    return this.http.patch<Response>('http://localhost:8080/api/users/me', {
      oldPassword: this.changePasswordForm.value.oldPassword,
      newPassword: this.changePasswordForm.value.newPassword,
    },{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem('jwt-token')
      })
    }).subscribe({
      next: (res) => {
        if (res.status === 200) {
          this.openSnackBar(res.message, 'Hide')
          setTimeout(() => this.closeSnackBar(), 3000);
          this.dialogRef.close()
        }
      },
      error: (err) => {
        this.openSnackBar(err.error.message,'Hide')
        setTimeout(() => this.closeSnackBar(), 3000)
      }
    })
  }

}
