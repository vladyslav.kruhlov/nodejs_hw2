import { Component, Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '../../models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

@Injectable()
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
  })

  constructor(
    private http: HttpClient,
    private _snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('jwt-token')) {
      this.router.navigate(['']);
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  closeSnackBar() {
    this._snackBar.dismiss()
  }

  onSubmit(): Subscription {

    return this.http.post<any>('http://localhost:8080/api/auth/login', {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password,
    }).subscribe({
      next: (res) => {
        if(res.status === 200) {
          console.log(res)
          localStorage.setItem('jwt-token', res.jwt_token);
          this.openSnackBar('User successfully sign in', 'Hide')
          setTimeout(() => this.closeSnackBar(), 3000)
          this.router.navigate([''])
        }
      },
      error: (err) => {
        this.openSnackBar('username or password is incorrect. try again', 'Hide')
        setTimeout(() => this.closeSnackBar(), 3000)
        this.loginForm.setValue({
          username: this.loginForm.value.username,
          password: '',
        })
      }
    })
  }

}
